<?php include 'ldap.php'; ?>

<!DOCTYPE html>
<html>
  <head>
    <title>Welcome to Glitch!</title>
    <meta name="description" content="A cool thing made with Glitch">
    <link id="favicon" rel="icon" href="https://glitch.com/edit/favicon-app.ico" type="image/x-icon">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <header>
      <h1>
	LDAP/Directory to CSV
      </h1>
    </header>

    <main>
      <p class="bold">Get full directory entries for multiple people.</p>
    
      <form action="to-csv.php" method="post">
        <input type="text" name="netids" placeholder="Feed me a comma separated list of NetID's.">
        <button type="submit">Go</button>
      </form>
<pre>
</pre>
      <section class="dreams">
        <textarea id="results">
<?php 
	$input = $_POST['netids'];
	if (isset($input)) {
		$records = get_multiple_entries($input);
		echo phonebook_to_csv($records); 
	}
?>
	</textarea>
      </section>
    </main>

  </body>
</html>
