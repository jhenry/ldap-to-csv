<?php
/**
 * @package LDAP Utilities
 * @version 0.1
 */
/*
Description: LDAP Utilities Library
Author: Justin Henry
Version: 0.1
Author URI: https://uvm.edu/~jhenry
*/


/**
 * Take a list of Netid's and return an array of ldap entries.
 * 
 * @param array $netids
 * @return array
 */

function get_multiple_entries($netids) {

	$netid_array = explode(',', $netids);
	foreach ( $netid_array as $netid ) {
		$phonebook[] = flatten_ldap_arrays(get_ldap_entry($netid));
	}
	
	return $phonebook;
}


/**
 * Convert directory multi-array to csv
 * 
 * @param array $phonebook
 * @return array
 */

function phonebook_to_csv($phonebook) {

	$attribute_names = "uid,mail,title,eduPersonPrimaryAffiliation,uvmEduPrimaryAffiliation,eduPersonAffiliation,uvmEduAffiliation,ou"; 
	$spreadsheet = $attribute_names;
	foreach ( $phonebook as $record ) {
		$row = implode(",", build_row($record, $attribute_names));
		$spreadsheet = $spreadsheet . "\n" . $row;
	}
	
	return $spreadsheet;
}



/**
 * Fill out a retrieved entry based on a list of desired attributes.
 * 
 * @param int $user_id
 * @return void
 */
function build_row($entry, $attribute_names) {
	$attributes = explode (",", $attribute_names);
	foreach($attributes as $key => $attribute){
		$person[$attribute] = $entry[$attribute] ?? NULL;		
	}
	
	return $person;
}


/**
 * Flatten/join arrays for entries with multiple items.
 * 
 * @param array $ldap_array
 * @return array
 */
function flatten_ldap_arrays($ldap_array) {
	foreach ( $ldap_array as &$item ) {
		if ( is_array($item)) {
			$item = implode( ";", $item);
		}
	}
	unset($item);
	return $ldap_array;
}



/**
 * Get an entry in LDAP for the specified username.
 * 
 * @param int $username
 * @return array
 */
function get_ldap_entry($username) {

	$filter = "netid=" . $username;
		
	// Connection and Base DN string configuration.
	$ldapserver = "ldaps://ldap.uvm.edu";
	$dnstring = "dc=uvm,dc=edu";
	
	$ds = ldap_connect($ldapserver);
	
	if ($ds) {
		// Bind (anonymously, no auth) and search
		$r = ldap_bind($ds);
		$sr = ldap_search($ds, $dnstring, $filter);

		// Kick out on a failed search.
		if(!ldap_count_entries($ds, $sr))
			return false;

		// Retrieve records found by the search
		$info = ldap_get_entries($ds, $sr);
		$entry = ldap_first_entry($ds, $sr);
		$attrs = ldap_get_attributes($ds, $entry);

		// Close the door on the way out.
		ldap_close($ds);

		//return $attrs;
		return cleanUpEntry($attrs);
	}


}

/**
 * Clean up and flatten an LDAP entry array.  
 * 
 * @param array $entry
 * @return array
 */
// Flatten LDAP entry array.
// https://secure.php.net/manual/en/function.ldap-get-entries.php#89508
function cleanUpEntry( $entry ) {
  $retEntry = array();
  for ( $i = 0; $i < $entry['count']; $i++ ) {
    if (is_array($entry[$i])) {
      $subtree = $entry[$i];
      //This condition should be superfluous so just take the recursive call
      //adapted to your situation in order to increase perf.
      if ( ! empty($subtree['dn']) and ! isset($retEntry[$subtree['dn']])) {
        $retEntry[$subtree['dn']] = cleanUpEntry($subtree);
      }
      else {
        $retEntry[] = cleanUpEntry($subtree);
      }
    }
    else {
      $attribute = $entry[$i];
      if ( $entry[$attribute]['count'] == 1 ) {
        $retEntry[$attribute] = $entry[$attribute][0];
      } else {
        for ( $j = 0; $j < $entry[$attribute]['count']; $j++ ) {
          $retEntry[$attribute][] = $entry[$attribute][$j];
        }
      }
    }
  }
  return $retEntry;
}

?>
